let express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    app = express();
//  list routes service
let routes = require('./app/routes/dataRoutes');

app.use(cors());

app.use(
    bodyParser.urlencoded({
        extended: true
    })
);

app.use(bodyParser.json());
app.use(routes);
app.use(express.static(__dirname));

module.exports = app;
