let express = require('express');
let router = express.Router();
let authApp = require('../middleware/authMiddleware');
let loginRoutes = require('../middleware/request/loginRequest');
// services
let locationServices = require('../services/locationServices');
let userServices = require('../services/loginServices');

router.get('/api', (req, res) => {
    return res.json({
        success: true,
        message: 'api test kasir pintar 1.0'
    });
});
router.post('/api/login', loginRoutes.login, userServices.login);
router.get('/api/user', authApp(), userServices.index);
router.get('/api/address/:id',  authApp('p'), locationServices.findById);
router.get('/api/list/address/:id',  authApp('p'), locationServices.searchMulti);
router.get('/api/all_address', authApp(), locationServices.index);

module.exports = router;