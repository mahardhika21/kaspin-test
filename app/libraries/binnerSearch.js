module.exports =  (dataArray, value) => {
    arr = dataArray.address_kecamatan;
    left = 0; 
    right = arr.length-1;

    mid = parseInt((left+right)/2);
    mid2 = mid + 1;
    stat = true;
    while (stat) {
            if(mid >= left){
                if (arr[mid].id == value) {
                    return arr[mid];
                }
                mid--;
            }
        
            if(mid2 <= right) {
                if(arr[mid2].id == value) {
                     return arr[mid2];
                }
                mid2++;
            }
           
            if (mid < left && mid2 > left) {
                stat = false;
            }
    }
    
    return [];
}