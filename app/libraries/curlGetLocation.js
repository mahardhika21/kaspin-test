const storageTmp = require('store');
const curl = require('axios');

module.exports = async () => {
    if (!storageTmp.get('location')) {
       await curl.get('https://kasirpintar.co.id/allAddress.txt').then(function(response){
            storageTmp.set('location', response.data);
       });
        return storageTmp.get('location');
    } else {
        return storageTmp.get('location');
    }
}