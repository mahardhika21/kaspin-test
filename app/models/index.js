const config = require("../config/config");
const { Sequelize, DataTypes, Op } = require("sequelize");

const sequelize = new Sequelize(
  config.DB_NAME,
  config.DB_USER,
  config.DB_PASS,
  {
    host: config.DB_HOST,
    dialect: 'mysql',
    operatorsAliases: 0,
    poll: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.Op = Op;
db.sequelize = sequelize;

db.users = require("./Users")(sequelize, Sequelize, DataTypes);

module.exports = db;
