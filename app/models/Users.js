let moment = require('moment');
module.exports = (sequelize, Sequelize, DataTypes) =>  {
    const Users = sequelize.define(
        "users",
        {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true
            },
            name: {
                type: DataTypes.STRING,
            },
            username: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            password: {
                type: DataTypes.STRING,
            },
            role: {
                type: DataTypes.STRING,
            },
            active: {
                type: DataTypes.STRING,
            },
            created_at: {
                allowNull: false,
                type: DataTypes.DATE,
                get() {
                        return moment(this.getDataValue('created_at')).format('YYYY-MM-DD h:mm:ss');
                }
            },
            updated_at: {
                allowNull: false,
                type: DataTypes.DATE
            }
        },
        {
            // Options
            timestamps: true,
            underscrored: true,
            freezeTableName: true,

            // define the table's name

            tableName: 'users',
            createdAt: "created_at",
            updatedAt: "updated_at"
        }
    );

      return Users;
}
