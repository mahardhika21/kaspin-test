async function login(req, res, next) {
    if (!req.body.username || !req.body.password) {
        return res.json({ success: false, message: 'username or password cannot empty' });
    }

    return next();
}

module.exports = { login }