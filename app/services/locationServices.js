const Looging = require('../libraries/logging');
let curlData = require('../libraries/curlGetLocation');
let binnerSearch = require('../libraries/binnerSearch');
let binnerMulti = require('../libraries/binnerMultiSearch');

async function index(req, res) {
    try {
        let data = await curlData();
        return res.json({
            success: true,
            message: 'success get data',
            data: data,
        });
    } catch (err) {
        Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied get data",
            log : err.message,
        });
    }
}

async function findById(req, res) {
    try {
         location = await curlData();
         data = await binnerSearch(location, req.params.id);

        return res.json({
            success: true,
            message: data.length > 0 ? "success get data by id" : 'data not fount',
            data : data
        });
    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied",
            log : err.message,
        });
    }
}

async function searchMulti(req, res) {
     try {
         location = await curlData();
         data = await binnerMulti(location, req.params.id);

        return res.json({
            success: true,
            message: data.length > 0 ? "success get data by kota_id" : 'data not fount',
            data : data
        });
    } catch (err) {
         Looging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied",
            log : err.message,
        });
    }
}


module.exports = {
    index, findById, searchMulti
}