const db = require('../models');
const config = require('../config/config');
const logging = require('../libraries/logging');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

async function index(req, res) {
    try {
       let user = await db.users.findOne({ where : { id: res.authUser.id } });
        return res.json({
            success: true,
            message: "succes get data",
            data : user
        })
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "falied get data",
            log : err.message,
        });
    }
}

async function login(req, res) {
    try {
        const payload = req.body;
        let loginData = await db.users.findOne({
            where: {
                username: payload.username,
                active: '1',
                password: crypto.createHash('sha256').update(payload.password).digest('hex'),
            }
        });

        if (loginData == undefined) {
            return res.status(401).json({
                success: false,
                message: 'user not found'
            });
        }

        if (parseInt(loginData.active) != 1) {
            return res.json({
                success: false,
                message: 'user not active'
            });
        }
        let token = jwt.sign({
            id: loginData.id,
            role: loginData.role,
            name : loginData.name,
        }, config.JWT_SECRET);

        return res.json({
            success: true,
            message: 'login success',
            token: token,
            role: loginData.role,
        });
    } catch (err) {
        logging.errorLog(err, req.connection.remoteAddress, req.get('User-Agent'), req.body, req.originalUrl);
        return res.json({
            success: false,
            message: "failed : err",
            log: err.stack
        });
    }
}

module.exports = {
    index, login
}